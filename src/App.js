import './App.css';
import { useState, useEffect } from 'react'

function App() {
  const [angka1, setAngka1] = useState(0)
  const [angka2, setAngka2] = useState(0)
  const [angka3, setAngka3] = useState(0)

  const [cek1, setCek1] = useState(true)
  const [cek2, setCek2] = useState(true)
  const [cek3, setCek3] = useState(true)

  const [nil1, setNil1] = useState(0)
  const [nil2, setNil2] = useState(0)
  const [nil3, setNil3] = useState(0)

  const [hasil, setHasil] = useState()
  const [jumcek, setJumcek] = useState("")

  const [operasi, setOperasi] = useState("")
  const [operasi1, setOperasi1] = useState("")

  const [jml, setJml] = useState(0)

  const handleChangeN1 = (event) => {
    setHasil(undefined)
    setAngka1(event.target.value)
  }

  const handleChangeN2 = (event) => {
    setHasil(undefined)
    setAngka2(event.target.value)
  }

  const handleChangeN3 = (event) => {
    setHasil(undefined)
    setAngka3(event.target.value)
  }

  const handleChangeC1 = (event) => {
    setHasil(undefined)
    setCek1(event.target.checked)
  }
  const handleChangeC2 = (event) => {
    setHasil(undefined)
    setCek2(event.target.checked)
  }
  const handleChangeC3 = (event) => {
    setHasil(undefined)
    setCek3(event.target.checked)
  }

  useEffect(() => {
    cek1 ? setNil1(1) : setNil1(0)
    cek2 ? setNil2(1) : setNil2(0)
    cek3 ? setNil3(1) : setNil3(0)

    let jumlah = nil1 + nil2 + nil3

    setJml(jumlah)

    if (jumlah === 0) {
      setJumcek("0 input")
    } else if (jumlah === 1) {
      setJumcek("1 input")
    } else if (jumlah === 2) {
      setJumcek("2 input")
    } else if (jumlah === 3) {
      setJumcek("semua input")
    }

  }, [cek1, cek2, cek3, nil1, nil2, nil3])

  const handleTambah = (event) => {
    if (jml > 1) {
      event.preventDefault()
      setOperasi("Tambah (+)")
      setOperasi1("+")
      setHasil((nil1 * angka1) + (nil2 * angka2) + (nil3 * angka3))
    } else {
      alert("Error")
    }
  }

  const handleKurang = (event) => {
    if (jml > 1) {
      event.preventDefault()
      setOperasi("Kurang (-)")
      setOperasi1("-")
      setHasil((nil1 * angka1) - (nil2 * angka2) - (nil3 * angka3))
    } else {
      alert("Error")
    }
  }

  const handleKali = (event) => {
    if (jml > 1) {
      event.preventDefault()
      setOperasi("Kali (*)")
      setOperasi1("*")
      if (nil1 === 0 && nil2 === 1 && nil3 === 1) {
        setHasil((nil2 * angka2) * (nil3 * angka3))
      } else if (nil2 === 0 && nil1 === 1 && nil3 === 1) {
        setHasil((nil1 * angka1) * (nil3 * angka3))
      } else if (nil3 === 0 && nil1 === 1 && nil2 === 1) {
        setHasil((nil1 * angka1) * (nil2 * angka2))
      } else {
        setHasil((nil1 * angka1) * (nil2 * angka2) * (nil3 * angka3))
      }
    } else {
      alert("Error")
    }
  }

  const handleBagi = (event) => {
    if (jml > 1) {
      event.preventDefault()
      setOperasi("Bagi (/)")
      setOperasi1("/")
      if (nil1 === 0 && nil2 === 1 && nil3 === 1) {
        setHasil((nil2 * angka2) / (nil3 * angka3))
      } else if (nil2 === 0 && nil1 === 1 && nil3 === 1) {
        setHasil((nil1 * angka1) / (nil3 * angka3))
      } else if (nil3 === 0 && nil1 === 1 && nil2 === 1) {
        setHasil((nil1 * angka1) / (nil2 * angka2))
      } else {
        setHasil((nil1 * angka1) / (nil2 * angka2) / (nil3 * angka3))
      }
    } else {
      alert("Error")
    }
  }

  return (
    <div>

      <div>
        <input type="number" value={angka1} onChange={handleChangeN1} required />
        <input type="checkbox" checked={cek1} onChange={handleChangeC1} />
      </div>
      <div>
        <input type="number" value={angka2} onChange={handleChangeN2} required />
        <input type="checkbox" checked={cek2} onChange={handleChangeC2} />
      </div>
      <div>
        <input type="number" value={angka3} onChange={handleChangeN3} required />
        <input type="checkbox" checked={cek3} onChange={handleChangeC3} />
      </div>
      <button onClick={handleTambah}>+</button>
      <button onClick={handleKurang}>-</button>
      <button onClick={handleKali}>*</button>
      <button onClick={handleBagi}>/</button>
      <hr />
      {hasil !== undefined && <div>
        <p>Hasil: {hasil}</p>
        <p>input 1: {angka1}</p>
        <p>input 2: {angka2}</p>
        <p>input 3: {angka3}</p>
        <p>checklist: {jumcek} </p>
        <p>operasi: {operasi} </p>
        <p>hasil:
          {
            cek1 ? " " + angka1 + "" + operasi1 : ""
          }
          {
            cek2 ? angka2 + "" + operasi1 : ""
          }
          {
            cek3 ? angka3 : ""
          }
          = {hasil}
        </p>
      </div>
      }
    </div>
  );
}

export default App;
